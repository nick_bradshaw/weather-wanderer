# Weather Wanderer README #


* **Quick summary**
  * Weather Wanderer is an app that allows you to quickly get current weather conditions for *wherever you may wander* based on data from [OpenWeatherMap.org](http://openweathermap.org) 

* ** Version:** This is the first iteration of the app- Acceptance Criteria was **"Can I see today's weather forecast for my current location?"**

### How do I get set up? ###
   * To get this app up and running clone this repo or download the zip file
     - Open up Android Studio and simply select from the Menu-  File > Import Project
   - Once you have the app open in Android Studio you will need to obtain an API Key to access the weather data
     - You can get an OpenWeatherMap API key from [OpenWeatherMap.org/appid](http://openweathermap.org/appid) 
     - Once you've obtained an API Key add it to you "build.gradle" file by replacing ***Your API Key here*** with your API Key:



```
#!python

    buildTypes.each {
        it.buildConfigField 'String', 'OPEN_WEATHER_MAP_API_KEY', "\"Your API Key here\""
    }
```


   * Once you've got that configured choose Run 'app' from the Run dropdown menu.

##Navigating the App##

* When you launch the app you will be presented with a dialog that requests permission for Weather Wanderer to access your location. Without that permission the app will not be able tell you the weather for your location so it's pretty crucial.
* Once permission has been granted the app displays the current date and uses an AsyncTask to query the Open Weather Map API based on your device's current latitude and longitude and display the corresponding current temperature outside and weather condition (light rain, clear, etc...).
* Another AsyncTask grabs the stock weather icons provided by the folks at Open Weather Map.
* The app automatically checks for your location so if you close it or navigate to other apps and come back to Weather Wanderer- you'll have the most up-to-date conditions for your location.

##Future Features##

* Given no time constraint I would have liked to:
* Use a third-party library like Picasso or Glide to retrieve and load icon images as they are much more efficient (project instructions were to avoid third-party libraries).
* Use higher resolution icons and allow user to chose between several icon packs.
* Add a simple PlacePicker using the Google Places API that would allow the user to select a different location and see the current Weather for said location.
* Implement instance state management when rotating the device so the activity doesn't restart and re-fetch data.
* A menu setting that implements a simple method to allow user to chose between metric and imperial units.

Thanks for checking everything out and enjoy!