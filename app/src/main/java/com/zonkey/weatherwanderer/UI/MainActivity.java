package com.zonkey.weatherwanderer.UI;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.zonkey.weatherwanderer.R;
import com.zonkey.weatherwanderer.location.LocationProvider;
import com.zonkey.weatherwanderer.models.Weather;
import com.zonkey.weatherwanderer.network.IconAsync;
import com.zonkey.weatherwanderer.network.WeatherAsync;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity implements WeatherAsync.WeatherInterface, LocationProvider.LocationProviderInterface, MainActivityInterface {

    LinearLayout mContentLinearLayout;
    ProgressBar mContentProgressBar;

    TextView mTodayLocationTextView;
    TextView mTodayDateTextview;
    TextView mTodayForecastTextView;
    TextView mTodayCurrentTemp;
    TextView mTodayCurrentTempLabel;
    ImageView mTodayCurrentIcon;
    TextView mErrorView;
    FloatingActionButton mFab;

    SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat("EEEE, MMMM dd");
    String mTodayFormattedString;
    Date mTodayDate;
    private LocationProvider mLocationProvider;
    private MainActivityPresenter mMainActivityPresenter = new MainActivityPresenter(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContentLinearLayout = (LinearLayout) findViewById(R.id.content_layout);
        mContentProgressBar = (ProgressBar) findViewById(R.id.content_progress_bar);

        mTodayLocationTextView = (TextView) findViewById(R.id.today_location_textview);
        mTodayDateTextview = (TextView) findViewById(R.id.today_date_textview);
        mTodayForecastTextView = (TextView) findViewById(R.id.today_forecast_textview);
        mTodayCurrentTemp = (TextView) findViewById(R.id.today_current_temp_textview);
        mTodayCurrentTempLabel = (TextView) findViewById(R.id.today_current_temp_label);
        mTodayCurrentIcon = (ImageView) findViewById(R.id.today_icon);
        mErrorView = (TextView) findViewById(R.id.error_textview);
        mFab = (FloatingActionButton) findViewById(R.id.fab);

        mLocationProvider = new LocationProvider(this);

        getDateInfo();
        getLocationInformation();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Check to ensure network connection is available
        mMainActivityPresenter.onCreate();

    }

    public void showErrorMessage(int stringResourceId) {
        mContentLinearLayout.setVisibility(View.GONE);
        mContentProgressBar.setVisibility(View.GONE);
        mErrorView.setVisibility(View.VISIBLE);
        mErrorView.setText(stringResourceId);
    }

    @Override
    protected void onResume() {
        getLocationInformation();
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }

    @Override
    public void onSuccess(Weather weather) {
        mMainActivityPresenter.onSuccess(weather);
    }

    public void updateWeatherUI(final Weather weather) {

        mContentProgressBar.setVisibility(View.GONE);
        mContentLinearLayout.setVisibility(View.VISIBLE);
        mErrorView.setVisibility(View.GONE);

        mTodayLocationTextView.setText(weather.name);
        mTodayDateTextview.setText(mTodayFormattedString);
        final String roundedTempString = getRoundedTemperatureString(weather);
        mTodayCurrentTemp.setText(roundedTempString);
        mTodayCurrentTempLabel.setText(R.string.current_temp_label);
        mTodayForecastTextView.setText(weather.description);
        new IconAsync(mTodayCurrentIcon).execute(weather.icon);

        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, getShareForecastString(roundedTempString, weather));
                sendIntent.setType("text/plain");
                sendIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.fab_subject));
                startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.error_no_location_permission)));
            }
        });
    }

    @NonNull
    private String getShareForecastString(String roundedTempString, Weather weather) {
        return String.format("Hey it's %s outside with %s so make sure you dress appropriately!", roundedTempString, weather.description);
    }


    private String getRoundedTemperatureString(Weather weather) {
        String formattedTemperature = String.valueOf(weather.temp);
        int roundedTemp = Math.round(Float.parseFloat(formattedTemperature));
        return String.format(getString(R.string.fahrenheit_format_string), String.valueOf(roundedTemp));
    }

    @Override
    public void onError() {
        mMainActivityPresenter.onError();
    }

    private void getDateInfo() {
        mTodayDate = Calendar.getInstance().getTime();
        mTodayFormattedString = mSimpleDateFormat.format(mTodayDate);
    }

    private void getLocationInformation() {
        mContentProgressBar.setVisibility(View.VISIBLE);
        mContentLinearLayout.setVisibility(View.GONE);
        ensureLocationPermissionGranted();
    }

    private void ensureLocationPermissionGranted() {
        if (!mLocationProvider.checkHasLocationPermission()) {
            mContentProgressBar.setVisibility(View.GONE);
            mErrorView.setText(R.string.error_no_location_permission);
            mErrorView.setVisibility(View.VISIBLE);
        } else {
            mLocationProvider.getLocation(this);
        }
    }

    @Override
    public void onLocationAvailable(Location location) {
        WeatherAsync fetchWeatherTask = new WeatherAsync(this);
        fetchWeatherTask.execute(location);
    }

    @Override
    public void onLocationNotAvailable() {
        showErrorMessage(R.string.no_network_connection);
    }

    @Override
    protected void onDestroy() {
        mLocationProvider.destroy();
        super.onDestroy();
    }
}
