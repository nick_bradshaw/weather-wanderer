package com.zonkey.weatherwanderer.UI;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.zonkey.weatherwanderer.R;
import com.zonkey.weatherwanderer.models.Weather;

/**
 * Created by nickbradshaw on 10/25/16.
 */

public class MainActivityPresenter {

    private MainActivityInterface mainActivityInterface;
    private Weather mWeather;


    MainActivityPresenter(MainActivityInterface mainActivityInterface) {
        this.mainActivityInterface = mainActivityInterface;
    }

    void onCreate() {
        ConnectivityManager mConnectivityManager = (ConnectivityManager)
                mainActivityInterface.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = mConnectivityManager.getActiveNetworkInfo();
        if (networkInfo == null || !networkInfo.isConnected()) {
            mainActivityInterface.showErrorMessage(R.string.no_network_connection);
        }
    }

    void onSuccess(Weather weather) {
        mWeather = weather;
        if (mWeather != null) {
            mainActivityInterface.updateWeatherUI(mWeather);
        } else {
            mainActivityInterface.showErrorMessage(R.string.no_network_connection);
        }
    }

    void onError() {
        mWeather = null;
        mainActivityInterface.showErrorMessage(R.string.no_network_connection);
    }
}
