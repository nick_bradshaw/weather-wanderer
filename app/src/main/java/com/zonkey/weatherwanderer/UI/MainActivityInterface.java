package com.zonkey.weatherwanderer.UI;

import com.zonkey.weatherwanderer.models.Weather;

/**
 * Created by nickbradshaw on 10/25/16.
 */

interface MainActivityInterface {


    Object getSystemService(String connectivityService);

    void showErrorMessage(int no_network_connection);

    void updateWeatherUI(Weather weather);

}
