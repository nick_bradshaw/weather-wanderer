package com.zonkey.weatherwanderer.location;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.lang.ref.WeakReference;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.os.Build.VERSION_CODES.M;
import static android.support.v4.app.ActivityCompat.requestPermissions;
import static android.support.v4.content.ContextCompat.checkSelfPermission;

public class LocationProvider implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private Activity mActivity;
    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private static final long UPDATE_INTERVAL = 60 * 1000;
    private static final long FASTEST_UPDATE_INTERVAL = 10 * 1000;
    private static final int LOCATION_REQUEST = 1;
    private Location location;
    private WeakReference<LocationProviderInterface> mLocatorWeakReference;


    public LocationProvider(Activity activity) {
        mActivity = activity;
        if (activity != null) {
            googleApiClient = new GoogleApiClient.Builder(activity)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            locationRequest = new LocationRequest();
            locationRequest.setInterval(UPDATE_INTERVAL);
            locationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL);
            locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
            googleApiClient.connect();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        requestLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {
        notifyIfLocationAvailable(location);
    }

    @Override
    public void onLocationChanged(Location location) {
        notifyIfLocationAvailable(location);
        this.location = location;
    }

    private void notifyIfLocationAvailable(Location location) {
        LocationProviderInterface locationProviderInterface = mLocatorWeakReference.get();
        if (locationProviderInterface != null) {
            if (location != null) {
                locationProviderInterface.onLocationAvailable(location);
            } else {
                locationProviderInterface.onLocationNotAvailable();
            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        notifyIfLocationAvailable(location);
    }

    private void requestLocation() {
        if (googleApiClient.isConnected()) {
            if (checkHasLocationPermission()) {
                LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
                LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            }
        }
    }

    public boolean checkHasLocationPermission() {
        if (Build.VERSION.SDK_INT >= M) {
            boolean locationPermissionGranted = checkSelfPermission(mActivity, ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
            if (!locationPermissionGranted) {
                requestPermissions(mActivity, new String[]{ACCESS_FINE_LOCATION}, LOCATION_REQUEST);
            }
            return locationPermissionGranted;
        }
        return true;
    }

    public Location getLocation(LocationProviderInterface locationProviderInterface) {
        mLocatorWeakReference = new WeakReference<>(locationProviderInterface);
        requestLocation();
        return location;
    }

    private void removeUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
    }

    public interface LocationProviderInterface {
        void onLocationAvailable(Location location);
        void onLocationNotAvailable();
    }

    public void destroy() {
        removeUpdates();
        mActivity = null;
    }
}