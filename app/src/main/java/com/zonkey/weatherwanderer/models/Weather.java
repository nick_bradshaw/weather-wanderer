package com.zonkey.weatherwanderer.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by nickbradshaw on 10/22/16.
 */

public class Weather implements Parcelable {

    //names of the JSON objects that need to be extracted
    private static final String OPEN_WEATH_COORD = "coord"; //parent CoordObject
    private static final String OPEN_WEATH_LON = "lon";
    private static final String OPEN_WEATH_LAT = "lat";

    private static final String OPEN_WEATH_WEATHER = "weather"; //parent weatherArray
    private static final String OPEN_WEATH_ID = "id";
    private static final String OPEN_WEATH_DESCRIPTION = "description";//simpler of two API descriptions
    private static final String OPEN_WEATH_ICON = "icon";

    private static final String OPEN_WEATH_MAIN = "main";  //Parent weatherValsObject
    private static final String OPEN_WEATH_TEMPERATURE = "temp";
    private static final String OPEN_WEATH_MAX = "temp_max";
    private static final String OPEN_WEATH_MIN = "temp_min";

    private static final String OPEN_WEATH_NAME = "name"; //name associated with coordinates


    public static Weather createFromJSON(String jsonString) throws JSONException {
        Weather weather = new Weather();

        JSONObject forecastJsonObject = new JSONObject(jsonString);
        JSONObject coordObject = forecastJsonObject.getJSONObject(OPEN_WEATH_COORD);
        weather.lat = coordObject.getString(OPEN_WEATH_LAT);
        weather.lon = coordObject.getString(OPEN_WEATH_LON);

        JSONArray weatherArray = forecastJsonObject.optJSONArray(OPEN_WEATH_WEATHER);
        for (int i = 0; i < weatherArray.length(); i++) {
            JSONObject weatherObject = weatherArray.getJSONObject(i);
            weather.description = weatherObject.getString(OPEN_WEATH_DESCRIPTION);
            weather.id = weatherObject.getString(OPEN_WEATH_ID);
            weather.icon = weatherObject.getString(OPEN_WEATH_ICON);
        }

        JSONObject weatherValsObject = forecastJsonObject.getJSONObject(OPEN_WEATH_MAIN);
        weather.temp = weatherValsObject.getString(OPEN_WEATH_TEMPERATURE);
        weather.temp_max = weatherValsObject.getString(OPEN_WEATH_MAX);
        weather.temp_min = weatherValsObject.getString(OPEN_WEATH_MIN);

        weather.name = forecastJsonObject.getString(OPEN_WEATH_NAME);

        return weather;

    }

    //Constructor
    private Weather() {
    }


    private String coord;
    private String lon;
    private String lat;
    private String weather;
    public String id;
    public String description;
    public String icon;
    private String main;
    public String temp;
    private String temp_max;
    private String temp_min;
    public String name;


    private Weather(Parcel in) {
        coord = in.readString();
        lon = in.readString();
        lat = in.readString();
        weather = in.readString();
        icon = in.readString();
        main = in.readString();
        temp = in.readString();
        temp_max = in.readString();
        temp_min = in.readString();
        name = in.readString();
    }

    public static final Creator<Weather> CREATOR = new Creator<Weather>() {
        @Override
        public Weather createFromParcel(Parcel in) {
            return new Weather(in);
        }

        @Override
        public Weather[] newArray(int size) {
            return new Weather[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(coord);
        parcel.writeString(lon);
        parcel.writeString(lat);
        parcel.writeString(weather);
        parcel.writeString(icon);
        parcel.writeString(main);
        parcel.writeString(temp);
        parcel.writeString(temp_max);
        parcel.writeString(temp_min);
        parcel.writeString(name);
    }
}