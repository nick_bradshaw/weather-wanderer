package com.zonkey.weatherwanderer.network;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URL;

/**
 * Created by nickbradshaw on 10/24/16.
 */

public class IconAsync extends AsyncTask<String, String, Bitmap> {
    private WeakReference<ImageView> mWeakReference;

    public IconAsync(ImageView imageView) {
        mWeakReference = new WeakReference<>(imageView);
    }

    @Override
    protected Bitmap doInBackground(String... args) {
        String iconBaseUrl = "http://openweathermap.org/img/w/%s.png";
        String urlString = String.format(iconBaseUrl, args[0]);

        Bitmap iconBitmap = null;
        try {
            iconBitmap = BitmapFactory.decodeStream((InputStream) new URL(urlString).getContent());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return iconBitmap;
    }

    @Override
    protected void onPostExecute(Bitmap result) {
        ImageView imgview = mWeakReference.get();

        if (imgview != null) {

            imgview.setImageBitmap(result);
        }

    }
}
