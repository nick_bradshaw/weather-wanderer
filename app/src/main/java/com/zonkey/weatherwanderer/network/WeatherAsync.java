package com.zonkey.weatherwanderer.network;

import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.zonkey.weatherwanderer.BuildConfig;
import com.zonkey.weatherwanderer.models.Weather;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by nickbradshaw on 10/21/16.
 */

public class WeatherAsync extends AsyncTask<Location, Void, Weather> {

    private final String LOG_TAG = WeatherAsync.class.getSimpleName();

    private WeakReference<WeatherInterface> mWeakReference;

    public WeatherAsync(WeatherInterface weatherInterface) {
        mWeakReference = new WeakReference<>(weatherInterface);
    }

    @Override
    protected Weather doInBackground(Location... params) {

        if (params.length == 0) {
            return null;
        }

        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        String forecastJsonString;
        Location location = params [0];

        double latitude = location.getLatitude();
        String latNumberAsString = String.valueOf(latitude);
        double longitude = location.getLongitude();
        String lonNumberAsString = String.valueOf(longitude);
        String units = "imperial";

        try {
            final String FORECAST_BASE_URL = "http://api.openweathermap.org/data/2.5/weather?";
            final String LAT_PARAM = "lat";
            final String LON_PARAM = "lon";
            final String UNITS_PARAM = "units";
            final String APPID_PARAM = "APPID";

            Uri builtUri = Uri.parse(FORECAST_BASE_URL).buildUpon()
                    .appendQueryParameter(LAT_PARAM, latNumberAsString)
                    .appendQueryParameter(LON_PARAM, lonNumberAsString)
                    .appendQueryParameter(UNITS_PARAM, units)
                    .appendQueryParameter(APPID_PARAM, BuildConfig.OPEN_WEATHER_MAP_API_KEY)
                    .build();

            URL url = new URL(builtUri.toString());

            Log.v(LOG_TAG, "Built URI equals " + builtUri.toString());

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            InputStream inputStream = urlConnection.getInputStream();
            StringBuilder buffer = new StringBuilder();
            if (inputStream == null) {
                return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line).append("\n");
            }
            if (buffer.length() == 0) {
                return null;
            }
            forecastJsonString = buffer.toString();
            return Weather.createFromJSON(forecastJsonString);

        } catch (IOException e) {
            return null;
        } catch (JSONException e) {
            return null;
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException ignored) {
                }
            }
        }
    }

    @Override
    protected void onPostExecute(Weather result) {
        WeatherInterface weatherInterface = mWeakReference.get();
        if (weatherInterface != null) {
            if (result != null) {
                weatherInterface.onSuccess(result);
            } else {
                weatherInterface.onError();
            }
        }
    }

    public interface WeatherInterface {
        void onSuccess(Weather weather);
        void onError();
    }
}
