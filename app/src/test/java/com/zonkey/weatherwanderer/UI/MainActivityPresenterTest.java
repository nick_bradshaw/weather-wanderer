package com.zonkey.weatherwanderer.UI;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.zonkey.weatherwanderer.models.Weather;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by nickbradshaw on 10/25/16.
 */

@RunWith(MockitoJUnitRunner.class)
public class MainActivityPresenterTest {

    @Mock
    MainActivityInterface testMainActivityInterface;

    @Mock
    ConnectivityManager testConnectivityManager;

    @Mock
    NetworkInfo testNetworkInfo;

    @Mock
    Weather mWeather;

    @InjectMocks
    MainActivityPresenter testMainActivityPresenter;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldShowNetworkErrorMessageWhenOnCreateCalledIfNoNetworkConnection() throws Exception {
        givenNoNetwork();
        whenOnCreateCalled();
        thenShowErrorMessage();
    }

    @Test
    public void shouldShowNetworkErrorMessageWhenOnCreateCalledIfNetworkInfoIsNull() throws Exception {
        givenNetworkInfoIsNull();
        whenOnCreateCalled();
        thenShowErrorMessage();
    }

    @Test
    public void shouldNotShowNetworkErrorMessageWhenOnCreateCalledIfNetworkIsConnected() throws Exception {
        givenAvailableNetwork();
        whenOnCreateCalled();
        thenDoNotShowErrorMessage();
    }

    @Test
    public void shouldUpdateWeatherUiWhenOnSuccessCalledAndWeatherIsNotNull() throws Exception {
        whenOnSuccessCalled(mWeather);
        thenUpdateWeatherUi();
    }

    @Test
    public void shouldShowErrorMessageWhenOnSuccessCalledAndWeatherIsNull() throws Exception {
        whenOnSuccessCalled(null);
        thenShowErrorMessage();

    }

    private void givenAvailableNetwork() {
        when(testMainActivityInterface.getSystemService(anyString())).thenReturn(testConnectivityManager);
        when(testConnectivityManager.getActiveNetworkInfo()).thenReturn(testNetworkInfo);
        when(testNetworkInfo.isConnected()).thenReturn(true);
    }

    private void givenNetworkInfoIsNull() {
        when(testMainActivityInterface.getSystemService(anyString())).thenReturn(testConnectivityManager);
        when(testConnectivityManager.getActiveNetworkInfo()).thenReturn(null);
    }

    private void givenNoNetwork() {
        when(testMainActivityInterface.getSystemService(anyString())).thenReturn(testConnectivityManager);
        when(testConnectivityManager.getActiveNetworkInfo()).thenReturn(testNetworkInfo);
        when(testNetworkInfo.isConnected()).thenReturn(false);
    }

    private void whenOnCreateCalled() {
        testMainActivityPresenter.onCreate();
    }

    private void whenOnSuccessCalled(Weather weather) {
        testMainActivityPresenter.onSuccess(weather);
    }

    private void thenShowErrorMessage() {
        verify(testMainActivityInterface).showErrorMessage(anyInt());
    }

    private void thenDoNotShowErrorMessage() {
        verify(testMainActivityInterface, never()).showErrorMessage(anyInt());
    }

    private void thenUpdateWeatherUi() {
        verify(testMainActivityInterface).updateWeatherUI(mWeather);
    }


}